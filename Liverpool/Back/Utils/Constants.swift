//
//  Constants.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation
import UIKit

struct colors {
    static let basePink      = UIColor(red: 1.00, green: 0.08, blue: 0.61, alpha: 1.00)
    static let secondaryPink = UIColor(red: 0.80, green: 0.06, blue: 0.49, alpha: 1.00)
    static let baseGray      = UIColor(red: 0.80, green: 0.80, blue: 0.80, alpha: 1.00)
    static let secondaryGray = UIColor(red: 0.26, green: 0.26, blue: 0.26, alpha: 1.00)
}

struct generalStrings {
    
    static let mainTitle     = "Bienvenido"
    static let productsTitle = "Productos"
    static let historyTitle = "Historial de búsqueda"
    static let accept    = "Aceptar"
    static let cancel    = "Cancelar"
    static let wait      = "Espere por favor..."
    static let history   = "Historial"
    
}

struct errorsStrings {
    
    static let errorTitle   = "Error"
    static let canceledByUser = "Cancelado por el usuario"
    static let emptyData    = "Todos los datos son necesarios"
    static let scanFail     = "Tu dispositivo no soporta el escaneo de códigos"
    static let badAccess    = "Usuario y/o contraseña invalido"
}
