//
//  Views.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation
import UIKit

class Views {
    static func activityIndicatorLoader(_ message:String) -> UIAlertController {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.tintColor = colors.secondaryPink
        
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: message as String, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 17.0)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: colors.secondaryPink, range: NSRange(location:0,length:message.count))
        alert.setValue(myMutableString, forKey: "attributedMessage")
        
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x:alert.view.frame.origin.x + 6, y: 15 , width: 30, height: 30)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = .gray
        loadingIndicator.startAnimating()
        loadingIndicator.color = colors.secondaryPink
        alert.view.tintColor = colors.secondaryPink
        alert.view.addSubview(loadingIndicator)
        return alert
    }
    
}
