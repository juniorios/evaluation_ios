//
//  Extensions.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation
import UIKit

enum StoryBoard:String {
    
    case main = "Main"
    
    func name() -> String {
        return self.rawValue
    }
}

extension UIStoryboard {
    
    static func type(type:StoryBoard) -> UIViewController {
        UIStoryboard(name: type.name(), bundle: nil).instantiateInitialViewController()!
    }
    
    static func type(type:StoryBoard, and identifier:String) -> UIViewController {
        UIStoryboard(name: type.name(), bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    
    static func overFullScreen(type:StoryBoard) -> UIViewController {
        let vc = UIStoryboard(name: type.name(), bundle: nil).instantiateInitialViewController()!
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        return vc
    }
    
    static func overFullScreen(type:StoryBoard, and identifier:String) -> UIViewController {
        let vc = UIStoryboard(name: type.name(), bundle: nil).instantiateViewController(withIdentifier: identifier)
        vc.modalPresentationStyle = .overFullScreen
        return vc
    }
    
}

extension Double {
   static func doubleToCurrency(amount:Double) -> String {
        return String(format: "%.2f", amount).currency()!
    }
}

extension String {
    
    func currency() -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.locale = Locale(identifier: "en_MX")
        formatter.currencySymbol = "$"
        let digits = NSDecimalNumber(string: sanitized())
        let place = NSDecimalNumber(value: powf(10, 2))
        return formatter.string(from: digits.dividing(by: place))
    }
    
    func sanitized() -> String {
        return filter { "01234567890".contains($0) }
    }
    
    
    
}
