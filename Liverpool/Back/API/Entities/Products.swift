//
//  Products.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation


struct Products: Decodable
{
    var pageType:String
    var PlpResults:plpResults
    
    enum CodingKeys: String, CodingKey {
        case pageType
        case plpResults
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        pageType = try values.decode(String.self, forKey: .pageType)
        PlpResults = try values.decode(plpResults.self, forKey: .plpResults)
    }
}

struct records: Codable {
    var productDisplayName:String
    var listPrice:Double
    var seller:String
    var smImage:String
    
    enum CodingKeys: String, CodingKey {
        case productDisplayName
        case listPrice
        case seller
        case smImage
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        productDisplayName = try values.decodeIfPresent(String.self, forKey: .productDisplayName) ?? ""
        listPrice = try values.decodeIfPresent(Double.self, forKey: .listPrice) ?? 0.00
        seller = try values.decodeIfPresent(String.self, forKey: .seller) ?? ""
        smImage = try values.decodeIfPresent(String.self, forKey: .smImage) ?? ""
    }
}
struct plpResults:Codable {
    var label:String
    var recordsList:[records]
    
    enum CodingKeys: String, CodingKey {
        case label
        case recordsList = "records"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        label = try values.decode(String.self, forKey: .label)
        recordsList = try values.decode([records].self, forKey: .recordsList)
    }
}
