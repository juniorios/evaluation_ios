//
//  ProductsGateway.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation

class ProductsGateway: APIClient {
    
    public func searchProducts(_ searchTerm:String,_ completion: @escaping (Result<Data,Error>)->Void){
        let request = ProductsRequest(searchTerm: searchTerm)
        excecute(request) { (result) in
            switch result {
            case .failure(let fail):
                completion(.failure(fail))
                return
            case .success(let response):
                if let data = response {
                   completion(.success(data))
                }
                return
            }
        }
        
    }
}
