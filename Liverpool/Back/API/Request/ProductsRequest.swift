//
//  ProductsRequest.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation

public struct ProductsRequest: ApiRequest {
    var searchTerm   = ""

    public var urlRequest: URLRequest {
        let urlString = ApiHost.urlBase + searchTerm.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! + ApiHost.liverpool
        let request = URLRequest(url: URL(string: urlString)!)
        return request
    }
}
