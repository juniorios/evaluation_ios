//
//  APIClient.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation

class APIClient: NSObject {
    
    var urlSession: URLSession!
    
    public override init() {
        super.init()
        urlSession = URLSession(configuration: URLSessionConfiguration.default,
                                delegate: nil,
                                delegateQueue: nil)
    }
    
    public func excecute(_ apiRequest:ApiRequest, _ completion: @escaping (Result<Data?,Error>)->Void) {
        let dataTask = urlSession.dataTask(with: apiRequest.urlRequest) { (data, response, fail) in
            
            guard fail == nil else {
                completion(.failure(fail!))
                return
            }
            
            guard data == nil else {
                completion(.success(data!))
                return
            }
            
        }
        dataTask.resume()
    }
}


public protocol ApiRequest {
    var urlRequest: URLRequest { get }
}

struct ApiHost {
    static let urlBase = "https://shoppapp.liverpool.com.mx/appclienteservices/services/v3/plp?force-plp=true&search-string="
    static let liverpool = "&page-number=1&number-of-items-per-page=50"
}
