//
//  SearchObject.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation
import RealmSwift

class SearchObject: Object {
    @objc dynamic var id = UUID().uuidString
    @objc dynamic var name = ""

    override static func primaryKey() -> String? {
        return "id"
    }
}

