//
//  DatabaseManager.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation
import RealmSwift


class DataBaseManager {
    private var database:Realm
    static let sharedInstance = DataBaseManager()
    
    private init() {
        database = try! Realm()
    }
    
    func addSearch(_ term:String){
        let search = SearchObject()
        search.name = term
        try! database.safeWrite {
            database.add(search)
        }
    }
    
    func getSearch()->[SearchObject]{
        return Array(self.database.objects(SearchObject.self))
    }

}

extension Realm {
    public func safeWrite(_ block: (() throws -> Void)) throws {
        if isInWriteTransaction {
            try block()
        } else {
            try write(block)
        }
    }
}
