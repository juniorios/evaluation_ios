//
//  ViewController.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    var presenter: MainPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        presenter?.toMainView()
        // Do any additional setup after loading the view.
    }
    
    func setup() {
        self.title = generalStrings.mainTitle
        presenter = MainPresenter(controller: self, router: MainRouter(controller: self))
    }


}

