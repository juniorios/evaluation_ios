//
//  SearchTableViewCell.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import UIKit
import Kingfisher

class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet var mainTitle: UILabel!
    @IBOutlet var mainPrice: UILabel!
    @IBOutlet var location: UILabel!
    @IBOutlet var productImage: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(_ rcd:records) {
        
        mainTitle.text = "Producto: \(rcd.productDisplayName)"
        mainPrice.text = "Precio: \(Double.doubleToCurrency(amount: rcd.listPrice))"
        location.text  = "Vendedor: \(rcd.seller)"
        let url = URL(string: rcd.smImage)!
        let resource = ImageResource(downloadURL: url)
        productImage!.kf.indicatorType = .activity
        productImage!.kf.setImage(with: resource)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
