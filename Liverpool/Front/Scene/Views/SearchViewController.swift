//
//  SearchViewController.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    var searchController: UISearchController?
    var presenter: SearchPresenter?
    var dataSource = [records]()
    
    fileprivate(set) lazy var emptyView: UIView = {
        guard let view = Bundle.main.loadNibNamed("Empty", owner: nil, options: [:])?.first as? UIView  else {
            return UIView()
        }
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupNavigationController()
        setupSeachController()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchController?.isActive = false
    }
    
    func setup(){
        self.title = generalStrings.productsTitle
        self.navigationItem.setHidesBackButton(true, animated: true)
        presenter = SearchPresenter(controller: self, router: SearchRouter(controller: self))
    }
    
    func setupNavigationController() {

        let history = UIBarButtonItem(title: generalStrings.history,
                                  style: .plain,
                                  target: self,
                                  action: #selector(onSearch))

        self.navigationItem.rightBarButtonItem = history
        
    }
    
    @objc func onSearch(){
        presenter?.toHistoryView()
    }
    
    func setupSeachController(){
        searchController = UISearchController(searchResultsController: nil)
        searchController?.hidesNavigationBarDuringPresentation = false
        searchController?.definesPresentationContext = true
        searchController?.searchBar.delegate = self
        searchController?.searchResultsUpdater = self
        searchController?.obscuresBackgroundDuringPresentation = false
        searchController?.searchBar.placeholder = "Buscar"
        tableView.tableHeaderView = searchController?.searchBar
    }
    

}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = dataSource.count == 0 ? emptyView : UIView()
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SearchTableViewCell
        cell.setData(dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
}

extension SearchViewController: UISearchResultsUpdating, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let seach = searchBar.text {
            presenter?.addNewSearch(seach)
            presenter?.searhProductsBy(seach, { (result) in
                switch result {
                case .success(let list):
                    self.dataSource = list
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                case .failure(_):
                    self.dataSource = [records]()
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            })
        }
        
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchController?.isActive = false
    }
    
    
}
