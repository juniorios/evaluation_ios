//
//  MainPresenter.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation

protocol MainPresenterProtocol {
    func toMainView()
}

class MainPresenter: MainPresenterProtocol {
    
    var router: MainRouter?
    weak var controller: MainViewController?
    
    init(controller: MainViewController, router:MainRouter) {
        self.controller = controller
        self.router = router
    }
    
    func toMainView() {
        router?.pushSearchViewController()
    }
    
    
}
