//
//  SearchPresenter.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation

protocol SearchPresenterProtocol {
    func toHistoryView()
}

class SearchPresenter: SearchPresenterProtocol {
    
    var router: SearchRouter?
    weak var controller: SearchViewController?
    
    init(controller: SearchViewController, router:SearchRouter) {
        self.controller = controller
        self.router = router
    }
    
    func searhProductsBy(_ searchTerm:String, _ completion: @escaping (Result<[records],Error>)->Void){
        ProductsGateway().searchProducts(searchTerm) { (result) in
            switch result {
            case .success(let response):
                let accessData = try? JSONDecoder().decode(Products.self, from: response)
                if let data = accessData {
                    completion(.success(data.PlpResults.recordsList))
                }
                return
            case .failure(let fail):
                completion(.failure(fail))
                return
            }
        }
    }
    
    func toHistoryView() {
        router?.pushSearchListViewController()
    }
    
    func addNewSearch(_ search:String){
        DataBaseManager.sharedInstance.addSearch(search)
    }
    
    
}
