//
//  SearchRouter.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation
import UIKit

protocol SearchViewRouter {
    func pushSearchListViewController()
}

class SearchRouter: SearchViewRouter {
    
    weak var controller: SearchViewController?
    
    init(controller: SearchViewController) {
        self.controller = controller
    }
    
    func pushSearchListViewController() {
        let viewController = UIStoryboard.overFullScreen(type: .main, and: "historyVC") as! HistoryTableViewController
        viewController.dataSource = DataBaseManager.sharedInstance.getSearch()
        self.controller?.navigationController?.pushViewController(viewController, animated: true)
    }
}
