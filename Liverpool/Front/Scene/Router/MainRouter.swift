//
//  MainRouter.swift
//  Liverpool
//
//  Created by Juan Jose Hernandez G on 17/09/20.
//  Copyright © 2020 Juan Jose Hernandez Galvan. All rights reserved.
//

import Foundation
import UIKit

protocol MainViewRouter {
    func pushSearchViewController()
}

class MainRouter: MainViewRouter {
    weak var controller: MainViewController?
    
    init(controller: MainViewController) {
        self.controller = controller
    }
    
    func pushSearchViewController() {
        let animation = Views.activityIndicatorLoader(generalStrings.wait)
        controller?.present(animation, animated: true, completion: {
            sleep(3)
            animation.dismiss(animated: true) {
                let viewController = UIStoryboard.overFullScreen(type: .main, and: "searchVC") as! SearchViewController
                self.controller?.navigationController?.pushViewController(viewController, animated: true)
            }
        })
    }
    
}
